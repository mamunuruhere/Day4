<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::prefix('admin')->group(function(){

	Route::get('/', 'DashboardController@home');

		Route::resource('/categories', 'CategoriesController');

		// Route::prefix('categories')->group(function(){

		// 	Route::get('/', 'CategoriesController@index');
		// 	Route::get('/create', 'CategoriesController@create');
		// 	Route::post('/', 'CategoriesController@store');
		// 	Route::get('/{id}', 'CategoriesController@show')->name('categories.show');
		// 	Route::get('/{id}/edit', 'CategoriesController@edit');
		// 	Route::put('/{id}', 'CategoriesController@update');
		// 	Route::delete('/{id}', 'CategoriesController@destroy');

		// });

});


// Route::get('/admin', function () {
//     return view('admin/dashboard');
// })->name('admin');

// Route::get('/admin/categories', function () {
//     return view('admin.categories.index');
// });

// Route::get('/admin/categories/create', function () {
//     return view('admin.categories.create');
// });

//Welcome Controller Route Start Here

Route::get('/', 'WelcomeController@page');
Route::get('/students', 'WelcomeController@students');
Route::get('/teachers', 'WelcomeController@teachers');
Route::get('/stuffs', 'WelcomeController@stuffs');
Route::get('/class', function () {
    return view('pages/class');
});
Route::get('/library', 'WelcomeController@library');
Route::get('/books', 'WelcomeController@books');
Route::get('/register', 'WelcomeController@register');


//Welcome Controller Route End Here


// Route::get('/', function () {
//     return view('pages/index');
// });

// Route::get('/students', function () {
//     return view('pages/students');
// });
// Route::get('/teachers', function () {
//     return view('pages/teachers');
// });
// Route::get('/stuffs', function () {
//     return view('pages/stuffs');
// });
// Route::get('/class', function () {
//     return view('pages/class');
// });
// Route::get('/library', function () {
//     return view('pages/library');
// });
// Route::get('/books', function () {
//     return view('pages/books');
// });
// Route::get('/register', function () {
//     return view('pages/register');
// });













