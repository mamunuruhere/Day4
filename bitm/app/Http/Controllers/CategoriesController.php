<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    
    public function index()
    
    {
        
        return view('admin.categories.index');

    }

    
    public function create()
    
    {
        
        return view('admin.categories.create');

    }
   
    public function store(Request $request)
    
    {
        
        dd($request->all());
        //echo "This is from store page.";
        //return view('admin.categories.create');

    }

    public function show($id)
    
    {
        
        echo "This is from show page: " . $id;

    }
 
    public function edit($id)
    
    {
        
        return view('admin.categories.edit');
        //echo "This is from edit page: " . $id;

    }

    public function update(Request $request, $id)
    
    {
        

        echo "This is from update page.";

    }

    public function destroy($id)
    
    {
        


    }
}
