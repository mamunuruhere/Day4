<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function page()
    
    {

    	return view('pages/index');

    }

    public function students()
    
    {

    	return view('pages/students');
    	
    }

    public function teachers()
    
    {

    	return view('pages/teachers');
    	
    }

    public function stuffs()
    
    {

    	return view('pages/stuffs');
    	
    }

    public function library()
    
    {

    	return view('pages/library');
    	
    }
    
    public function books()
    
    {

    	return view('pages/books');
    	
    }
    
    public function register()
    
    {

    	return view('pages/register');
    	
    }
}

