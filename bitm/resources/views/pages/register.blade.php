@extends('layouts.master')

@section('title','Register')

@include('layouts.navbar.nav_register')

@section('breadcrumb','Register')

@section('page_title','STUDENT REGISTRATION FORM')

@section('formcontent')

<div class="container">


{!! Form::open(['url' => '', 'method' => 'post']) !!}

<div class="form-group">
	{!! Form::label('nameField', 'Name') !!}
	{!! Form::text('name', null, ['class' => 'form-control', 'id' => 'nameField', 'placeholder' => 'Enter your name']) !!}

</div>

<div class="form-group">
	{!! Form::label('emailField', 'E-mail') !!}
	{!! Form::text('email', null, ['class' => 'form-control', 'id' => 'emailField', 'placeholder' => 'Enter your email']) !!}

</div>

<div class="form-group">
	
	{!! Form::submit('REGISTER', ['class' => 'btn btn-success']) !!}

</div>

{!! Form::close() !!}

<!-- <form action="" method="">
	<div class='form-group'>
		<label>Name</label>
		<input type='text' name="name" id='' class="form-control">
	</div>
	<div class='form-group'>
		<label>Email</label>
		<input type='text' name="name" id='' class="form-control">
	</div>
	<div class='form-group'>
		
		<button type='submit' class="btn btn-success">REGISTER </button>
	</div>

</form> -->
</div>
@endsection


