@extends('layouts.master')
@section('title','Books')
@include('layouts.navbar.nav_books')
@section('breadcrumb','Books')
@section('page_title','BOOKS')
@section('image_content')
<div class='container'>
	<div class='row'>		
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img1.jpg')}}" class="img-thumbnail" height="300px" width="100%">Book-1</p>
			</div>
		</div>
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img1.jpg')}}" class="img-thumbnail" height="300px" width="100%">Book-2</p>
			</div>
		</div>
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img1.jpg')}}" class="img-thumbnail" height="300px" width="100%">Book-3</p>
			</div>
		</div>
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img1.jpg')}}" class="img-thumbnail" height="300px" width="100%">Book-4</p>
			</div>
		</div>
	</div>
	<div class='row'>		
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img1.jpg')}}" class="img-thumbnail" height="300px" width="100%">Book-1</p>
			</div>
		</div>
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img1.jpg')}}" class="img-thumbnail" height="300px" width="100%">Book-2</p>
			</div>
		</div>
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img1.jpg')}}" class="img-thumbnail" height="300px" width="100%">Book-3</p>
			</div>
		</div>
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img1.jpg')}}" class="img-thumbnail" height="300px" width="100%">Book-4</p>
			</div>
		</div>
	</div>
	<div class='row'>		
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img1.jpg')}}" class="img-thumbnail" height="300px" width="100%">Book-1</p>
			</div>
		</div>
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img1.jpg')}}" class="img-thumbnail" height="300px" width="100%">Book-2</p>
			</div>
		</div>
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img1.jpg')}}" class="img-thumbnail" height="300px" width="100%">Book-3</p>
			</div>
		</div>
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img1.jpg')}}" class="img-thumbnail" height="300px" width="100%">Book-4</p>
			</div>
		</div>
	</div>
	<div class='row'>		
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img1.jpg')}}" class="img-thumbnail" height="300px" width="100%">Book-1</p>
			</div>
		</div>
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img1.jpg')}}" class="img-thumbnail" height="300px" width="100%">Book-2</p>
			</div>
		</div>
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img1.jpg')}}" class="img-thumbnail" height="300px" width="100%">Book-3</p>
			</div>
		</div>
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img1.jpg')}}" class="img-thumbnail" height="300px" width="100%">Book-4</p>
			</div>
		</div>
	</div>
</div>
@endsection