@extends('layouts.master')

@section('title','Teachers')

@include('layouts.navbar.nav_teachers')

@section('breadcrumb','Teachers')

@section('page_title','TEACHER')

@section('image_content')
<div class='container'>
	<div class='row'>		
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img3.jpg')}}" class="img-thumbnail" height="300px" width="100%">Flower-1</p>
			</div>
		</div>
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img3.jpg')}}" class="img-thumbnail" height="300px" width="100%">Flower-2</p>
			</div>
		</div>
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img3.jpg')}}" class="img-thumbnail" height="300px" width="100%">Flower-3</p>
			</div>
		</div>
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img3.jpg')}}" class="img-thumbnail" height="300px" width="100%">Flower-4</p>
			</div>
		</div>
	</div>
	<div class='row'>		
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img3.jpg')}}" class="img-thumbnail" height="300px" width="100%">Flower-1</p>
			</div>
		</div>
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img3.jpg')}}" class="img-thumbnail" height="300px" width="100%">Flower-2</p>
			</div>
		</div>
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img3.jpg')}}" class="img-thumbnail" height="300px" width="100%">Flower-3</p>
			</div>
		</div>
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img3.jpg')}}" class="img-thumbnail" height="300px" width="100%">Flower-4</p>
			</div>
		</div>
	</div>
	<div class='row'>		
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img3.jpg')}}" class="img-thumbnail" height="300px" width="100%">Flower-1</p>
			</div>
		</div>
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img3.jpg')}}" class="img-thumbnail" height="300px" width="100%">Flower-2</p>
			</div>
		</div>
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img3.jpg')}}" class="img-thumbnail" height="300px" width="100%">Flower-3</p>
			</div>
		</div>
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img3.jpg')}}" class="img-thumbnail" height="300px" width="100%">Flower-4</p>
			</div>
		</div>
	</div>
	<div class='row'>		
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img3.jpg')}}" class="img-thumbnail" height="300px" width="100%">Flower-1</p>
			</div>
		</div>
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img3.jpg')}}" class="img-thumbnail" height="300px" width="100%">Flower-2</p>
			</div>
		</div>
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img3.jpg')}}" class="img-thumbnail" height="300px" width="100%">Flower-3</p>
			</div>
		</div>
		<div class="col-xl-3 col-md-3 col-sm-6 col-xs-12 img-responsive">
			<div class="text-justify">
				<p><img src="{{asset('frontend/images/img3.jpg')}}" class="img-thumbnail" height="300px" width="100%">Flower-4</p>
			</div>
		</div>
	</div>
</div>
@endsection