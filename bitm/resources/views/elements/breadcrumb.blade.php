<div class='container breadcrumbstyle'>
<ol class="breadcrumb">
  <li><a href="{{'/'}}">Home</a></li>
  <li class="active">@yield('breadcrumb')</li>
</ol>
<h3>@yield('page_title')</h3>
</div>