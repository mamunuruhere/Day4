
<nav class="navbar navbar-default">
  	<div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    	<div class="navbar-header">
	      	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
	        	<span class="sr-only"></span>
	        	<span class="icon-bar"></span>
	        	<span class="icon-bar"></span>
	        	<span class="icon-bar"></span>
	      	</button>
      		<a class="navbar-brand" href="#">LOGO</a>
    	</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
   		<div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
      		<ul class="nav navbar-nav">
        		<li><a href="{{url('/')}}">HOME</a></li>
        		<li><a href="{{url('/admin')}}">DASHBOARD</a></li>
       		</ul>
       		<ul class="nav navbar-nav navbar-right">
		        <li><a href="{{url('/students')}}">STUDENTS</a></li>
		        <li><a href="{{url('/teachers')}}">TEACHERS</a></li>
		        <li class="active"><a href="{{url('/stuffs')}}">STUFFS</a></li>
		        <li><a href="{{url('/class')}}">CLASS</a></li>
		        <li><a href="{{url('/library')}}">LIBRARY</a></li>
		        <li><a href="{{url('/books')}}">BOOKS</a></li>
		        <li><a href="{{url('/register')}}">REGISTER</a></li>
      		</ul>
    	</div><!-- /.navbar-collapse -->
  	</div><!-- /.container-fluid -->
</nav>

