<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title')</title>
   
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{asset('frontend/css/bootstrap.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/css/font-awesome.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/css/bitmstyle.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/css/theme/shapla.css')}}">
    </head>  
    <body>