@extends('admin.layouts.master')

@section('page_title', 'Category Edit Page')

@section('content')
  
{!! Form::open(['url' => 'admin/categories/1', 'method' => 'put']) !!}

<div class="form-group">
	{!! Form::label('titleField', 'Title') !!}
	{!! Form::text('title', null, ['class' => 'form-control', 'id' => 'titleField', 'placeholder' => 'Enter Category Title']) !!}

</div>

<!-- <div class="form-group">
	{!! Form::label('emailField', 'E-mail') !!}
	{!! Form::text('email', null, ['class' => 'form-control', 'id' => 'emailField', 'placeholder' => 'Enter your email']) !!}

</div> -->

<div class="form-group">
	
	{!! Form::submit('UPDATE', ['class' => 'btn btn-success']) !!}

</div>

{!! Form::close() !!}

@endsection