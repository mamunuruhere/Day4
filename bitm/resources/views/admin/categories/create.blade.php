@extends('admin.layouts.master')

@section('page_title', 'Category Create Page')

@section('content')
  
{!! Form::open(['url' => 'admin/categories', 'method' => 'post']) !!}

<div class="form-group">
	{!! Form::label('titleField', 'Title') !!}
	{!! Form::text('title', null, ['class' => 'form-control', 'id' => 'titleField', 'placeholder' => 'Enter Category Title']) !!}

</div>

<!-- <div class="form-group">
	{!! Form::label('emailField', 'E-mail') !!}
	{!! Form::text('email', null, ['class' => 'form-control', 'id' => 'emailField', 'placeholder' => 'Enter your email']) !!}

</div> -->

<div class="form-group">
	
	{!! Form::submit('REGISTER', ['class' => 'btn btn-success']) !!}

</div>

{!! Form::close() !!}

@endsection